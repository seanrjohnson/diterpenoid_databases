# Data related to diterpenoids, compiled by the Hamberger laboratory at Michigan State University
**lamiaceae_skeleton_images**: contains png images of diterpenoid skeletons reported from Lamiaceae

**hamberger_lab_msu_terpene_references.msl**: a library of reference mass spectra for diterpenoids. Most spectra are derived from GC-MS data of extracts from Nicotiana benthamiana transiently expressing combinations of diterpene synthases. All spectra are background subtracted using the AMDIS deconvolution algorithm.

